import {FC} from "react";
import {Form, Formik} from "formik";
import InputField from "../components/InputField";
import {Button} from "@chakra-ui/react";
import TextAreaField from "../components/TextAreaField";
import {useRouter} from "next/router";
import {withUrqlClient} from "next-urql";
import {createUrqlCLient} from "../utils/createUrqlCLient";
import {useCreatePostMutation} from "../generated/graphql";
import Layout from "../components/Layout";
import {useIsAuth} from "../utils/useIsAuth";

const initVals = {
  title: '',
  text: '',
};

const CreatePost: FC<{}> = () => {
  useIsAuth();
  const router = useRouter();
  const [, createPost] = useCreatePostMutation()

  return (
    <Layout variant={"small"}>
      <Formik
        initialValues={initVals}
        onSubmit={async (values) => {
          const {error} = await createPost({input: values})
          if (!error) {
            router.push('/')
          }
        }}
      >
        {({isSubmitting}) => (
          <Form>
            <InputField name={"title"}/>
            <TextAreaField name={"text"} placeholder={'text...'}/>
            <Button
              type={"submit"}
              colorScheme={"teal"}
              mt={4}
              mx={"auto"}
              isLoading={isSubmitting}
            >
              create post
            </Button>
          </Form>
        )}
      </Formik>
    </Layout>
  );
};

export default withUrqlClient(createUrqlCLient)(CreatePost);
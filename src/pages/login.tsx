import {FC} from "react";
import {Form, Formik} from "formik";
import {Button, Flex, Link} from "@chakra-ui/react";
import Wrapper from "../components/Wrapper";
import InputField from "../components/InputField";
import {useLoginMutation} from "../generated/graphql";
import {toErrorMap} from "../utils/toErrorMap";
import {useRouter} from "next/router";
import {withUrqlClient} from "next-urql";
import {createUrqlCLient} from "../utils/createUrqlCLient";
import NextLink from "next/link";

const initVals = {
  usernameOrEmail: "",
  password: "",
};

const Login: FC<{}> = () => {
  const router = useRouter();
  const [, login] = useLoginMutation();
  return (
    <Wrapper>
      <Formik
        initialValues={initVals}
        onSubmit={async (values, {setErrors}) => {
          const response = await login(values);
          if (response.data?.login.errors) {
            setErrors(toErrorMap(response.data.login.errors))
          } else if (response.data?.login.user) {
            if (typeof router.query.next === "string") {
              router.push(router.query.next)
            } else {
              router.push("/")
            }
          }
        }}
      >
        {({isSubmitting}) => (
          <Form>
            <InputField name={"usernameOrEmail"} label={"Username or Email"} placeholder={"username or email"}/>
            <InputField name={"password"} type={"password"}/>
            <Flex mt={2}>
              <NextLink href={'/forgot-password'}>
                <Link ml={'auto'}>forgot password?</Link>
              </NextLink>
            </Flex>
            <Button
              type={"submit"}
              colorScheme={"teal"}
              mt={4}
              mx={"auto"}
              isLoading={isSubmitting}
            >
              login
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

export default withUrqlClient(createUrqlCLient)(Login);

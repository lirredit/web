import {FC} from "react";
import {Form, Formik} from "formik";
import {Button} from "@chakra-ui/react";
import Wrapper from "../components/Wrapper";
import InputField from "../components/InputField";
import {useRegisterMutation} from "../generated/graphql";
import {toErrorMap} from "../utils/toErrorMap";
import {useRouter} from "next/router";
import {withUrqlClient} from "next-urql";
import {createUrqlCLient} from "../utils/createUrqlCLient";

interface RegisterProps {
}

const initVals = {
  username: "",
  password: "",
  email: "",
};

const Register: FC<RegisterProps> = () => {
  const router = useRouter();
  const [, register] = useRegisterMutation();
  return (
    <Wrapper>
      <Formik
        initialValues={initVals}
        onSubmit={async (values, {setErrors}) => {
          const response = await register({options: values});
          if (response.data?.register.errors) {
            setErrors(toErrorMap(response.data.register.errors))
          } else if (response.data?.register.user) {
            router.push("/")
          }
        }}
      >
        {({isSubmitting}) => (
          <Form>
            <InputField name={"username"}/>
            <InputField name={"email"} type={"email"}/>
            <InputField name={"password"} type={"password"}/>
            <Button
              type={"submit"}
              colorScheme={"teal"}
              mt={4}
              mx={"auto"}
              isLoading={isSubmitting}
            >
              Confirm
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

export default withUrqlClient(createUrqlCLient)(Register);


import {NextPage} from "next";
import {Form, Formik} from "formik";
import {toErrorMap} from "../../utils/toErrorMap";
import InputField from "../../components/InputField";
import {Box, Button, Flex, Link} from "@chakra-ui/react";
import Wrapper from "../../components/Wrapper";
import {useChangePasswordMutation} from "../../generated/graphql";
import {useRouter} from "next/router";
import {useState} from "react";
import {createUrqlCLient} from "../../utils/createUrqlCLient";
import {withUrqlClient} from "next-urql";
import NextLink from "next/link";

const initVals = {
  newPassword: "",
};

const ChangePassword: NextPage = () => {
  const router = useRouter();
  const [, changePassword] = useChangePasswordMutation();
  const [tokenErr, setTokenErr] = useState('');
  return (
    <Wrapper>
      <Formik
        initialValues={initVals}
        onSubmit={async (values, {setErrors}) => {
          const response = await changePassword({
            newPassword: values.newPassword,
            token: typeof router.query.token === "string" ? router.query.token : ""
        });
          if (response.data?.changePassword.errors) {
            const errorMap = toErrorMap(response.data.changePassword.errors)
            if ("token" in errorMap) {
              setTokenErr(errorMap.token)
            }
            setErrors(errorMap)
          } else if (response.data?.changePassword.user) {
            router.push("/")
          }
        }}
      >
        {({isSubmitting}) => (
          <Form>
            <InputField name={"newPassword"} label={"New Password"} placeholder={"New password"} type={"password"}/>
            {tokenErr && (
              <Flex>
                <Box mr={2} color={'red'}>{tokenErr}</Box>
                <NextLink href={'/forgot-password'}>
                  <Link>click here to get a new one</Link>
                </NextLink>
              </Flex>
            )}
            <Button
              type={"submit"}
              colorScheme={"teal"}
              mt={4}
              mx={"auto"}
              isLoading={isSubmitting}
            >
              change password
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

export default withUrqlClient(createUrqlCLient)(ChangePassword);
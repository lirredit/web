import {FC} from "react";
import {withUrqlClient} from "next-urql";
import {createUrqlCLient} from "../../../utils/createUrqlCLient";
import {Form, Formik} from "formik";
import InputField from "../../../components/InputField";
import TextAreaField from "../../../components/TextAreaField";
import {Button} from "@chakra-ui/react";
import Layout from "../../../components/Layout";
import {useGetPostFromUrl} from "../../../utils/useGetPostFromUrl";
import {useUpdatePostMutation} from "../../../generated/graphql";
import {useGetIntId} from "../../../utils/useGetIntId";
import {useRouter} from "next/router";

interface EditPostProps {

}

const EditPost: FC<EditPostProps> = () => {
  const router = useRouter();
  const intId = useGetIntId();
  const [{data, fetching}] = useGetPostFromUrl();
  const [, updatePost] = useUpdatePostMutation();
  if (fetching) {
    return (
      <Layout>
        <div>loading...</div>
      </Layout>
    )
  }
  return (
    <Layout variant={"small"}>
      <Formik
        initialValues={{
          title: data?.post?.title || "",
          text: data?.post?.text || "",
        }}
        onSubmit={async (values) => {
          await updatePost({id: intId, ...values})
          router.push('/')
        }}
      >
        {({isSubmitting}) => (
          <Form>
            <InputField name={"title"}/>
            <TextAreaField name={"text"} placeholder={'text...'}/>
            <Button
              type={"submit"}
              colorScheme={"teal"}
              mt={4}
              mx={"auto"}
              isLoading={isSubmitting}
            >
              Update post
            </Button>
          </Form>
        )}
      </Formik>
    </Layout>
  );
};

export default withUrqlClient(createUrqlCLient)(EditPost);
import {withUrqlClient} from "next-urql";
import {createUrqlCLient} from "../../utils/createUrqlCLient";
import Layout from "../../components/Layout";
import {FC} from "react";
import {useGetPostFromUrl} from "../../utils/useGetPostFromUrl";

interface Props {
}

const Post: FC<Props> = () => {
  const [{data, error, fetching}] = useGetPostFromUrl();

  if (fetching) {
    return (
      <Layout>
        <div>loading..</div>
      </Layout>
    )
  }
  if (error) {
    return (<div>{error.message}</div>)
  }

  return (
    <Layout>
      {data?.post?.text}
    </Layout>
  );
};

export default withUrqlClient(createUrqlCLient)(Post);
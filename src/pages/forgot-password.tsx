import {FC, useState} from "react";
import {Form, Formik} from "formik";
import InputField from "../components/InputField";
import {Box, Button} from "@chakra-ui/react";
import Wrapper from "../components/Wrapper";
import {withUrqlClient} from "next-urql";
import {createUrqlCLient} from "../utils/createUrqlCLient";
import {useForgotPasswordMutation} from "../generated/graphql";

const initVals = {
  email: ''
};

const ForgotPassword: FC = () => {
  const [, forgotPassword] = useForgotPasswordMutation();
  const [complete, setComplete] = useState(false);

  return (
    <Wrapper>
      <Formik
        initialValues={initVals}
        onSubmit={async (values) => {
          await forgotPassword(values);
          setComplete(true)
        }}
      >
        {({isSubmitting}) => complete ? (
          <Box>in an account with that email exists, we sent you can email</Box>
        ) : (
          <Form>
            <InputField name={"email"} type={"email"}/>
            <Button
              type={"submit"}
              colorScheme={"teal"}
              mt={4}
              mx={"auto"}
              isLoading={isSubmitting}
            >
              forgot password
            </Button>
          </Form>
        )}
      </Formik>
    </Wrapper>
  );
};

export default withUrqlClient(createUrqlCLient)(ForgotPassword);
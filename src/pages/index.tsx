import {createUrqlCLient} from "../utils/createUrqlCLient";
import {withUrqlClient} from "next-urql";
import {useMeQuery, usePostsQuery} from "../generated/graphql";
import Layout from "../components/Layout";
import {Button, Flex, Heading, Link, Stack, Text} from "@chakra-ui/react";
import NextLink from "next/link";
import {useState} from "react";
import UpdootSection from "../components/UpdootSection";
import EditDeletePostButtons from "../components/EditDeletePostButtons";

const Index = () => {
  const [variables, setVariables] = useState({limit: 15, cursor: null as null | string})
  const [{data: meData}] = useMeQuery();
  const [{data, fetching}] = usePostsQuery({
    variables,
  });

  if (!data && !fetching) {
    return <div>oops query is dropped</div>
  }

  return (
    <Layout variant={"regular"}>
      <Stack spacing={4}>
        {data && (
          <>
            {data.posts.posts.map(p => !p ? null : (
              <Flex key={p.id} p={5} shadow={"md"} borderWidth={"1px"}>
                <UpdootSection post={p}/>
                <Flex flexGrow={1} justifyContent={"space-between"} alignItems={"center"}>

                  <Stack>
                    <NextLink href={`post/${p.id}`}>
                      <Link>
                        <Heading fontSize={"xl"}>{p.title}</Heading>
                      </Link>
                    </NextLink>
                    <Text>posted by: {p.creator.username}</Text>
                    <Text mt={4}>{p.textSnippet + "..."}</Text>
                  </Stack>
                  <EditDeletePostButtons
                    id={p.id}
                    disableBtns={{
                      delete: meData?.me?.id !== p.creator.id,
                      edit: meData?.me?.id !== p.creator.id,
                    }}/>
                </Flex>
              </Flex>
            ))}
            {data.posts.hasMore && (
              <Flex justifyContent={'center'}>
                <Button my={4} isLoading={fetching} onClick={() => setVariables({
                  limit: variables.limit,
                  cursor: data.posts.posts[data.posts.posts.length - 1].createdAt
                })}>load more</Button>
              </Flex>
            )}

          </>)
        }
        {fetching && (
          <div>loading...</div>
        )}
      </Stack>
    </Layout>
  );
}

export default withUrqlClient(createUrqlCLient, {ssr: true})(Index)

import {FC} from "react";
import {Box, Button, Flex, Heading, Link} from "@chakra-ui/react";
import NextLink from "next/link";
import {useLogoutMutation, useMeQuery} from "../generated/graphql";
import {isServer} from "../utils/isServer";
import {useRouter} from "next/router";

interface NavBarProps {

}

const NavBar: FC<NavBarProps> = ({}) => {
  const router = useRouter()
  const [{fetching: logoutFetching}, logout] = useLogoutMutation();
  const [{data, fetching}] = useMeQuery({pause: isServer()})
  let body = null;

  if (fetching) {

  } else if (!data?.me) {
    body = (
      <>
        <NextLink href={'login'}>
          <Link mr={2}>login</Link>
        </NextLink>
        <NextLink href={'register'}>
          <Link>register</Link>
        </NextLink>
      </>
    )
  } else {
    body = (
      <Flex justifyContent={"space-between"} alignItems={'center'}>
        <NextLink href={`/`}>
          <Link>
            <Heading fontSize={"xl"}>LiReddit</Heading>
          </Link>
        </NextLink>
        <Flex>
          <NextLink href={'/create-post'} >
            <Link mr={5}>create post</Link>
          </NextLink>
          <Box mr={2} color={"inherit"}>{data.me.username}</Box>
          <Button variant={'link'}
                  color={'inherit'}
                  onClick={async () => {
                    await logout();
                    router.reload()
                  }} isLoading={logoutFetching}
          >
            logout
          </Button>
        </Flex>
      </Flex>
    )
  }


  return (
    <Flex bg={'blue.600'} p={4} position={"sticky"} top={0} zIndex={1000}>
      <Box textColor={"white"} width={"100%"}>
        {body}
      </Box>
    </Flex>
  );
};

export default NavBar;
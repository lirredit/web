import {FC, InputHTMLAttributes, useState} from "react";
import {useField} from "formik";
import {FormControl, FormErrorMessage, FormLabel, Input} from "@chakra-ui/react";

type InputFieldProps = InputHTMLAttributes<HTMLInputElement> & {
  name: string;
  label?: string;
  placeholder?: string;
};

const InputField: FC<InputFieldProps> = (props) => {
  //Working only in Formik form
  const [{name, placeholder, label, restProps}] = useState(() => {
    const {
      label = props.name[0].toUpperCase() + props.name.slice(1),
      placeholder = props.name,
      size: _,
      ...restProps
    } = props;
    return {name: props.name, label, placeholder, restProps};
  });

  const [field, {error}] = useField(props);

  return (
    <FormControl isInvalid={!!error}>
      <FormLabel htmlFor={name}>{label}</FormLabel>
      <Input {...field} {...restProps} id={name} placeholder={placeholder}/>
      {error && <FormErrorMessage>{error}</FormErrorMessage>}
    </FormControl>
  );
};

export default InputField;

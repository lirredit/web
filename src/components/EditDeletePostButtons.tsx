import {FC} from "react";
import NextLink from "next/link";
import {IconButton, Stack} from "@chakra-ui/react";
import {DeleteIcon, EditIcon} from "@chakra-ui/icons";
import {useDeletePostMutation} from "../generated/graphql";

interface EditDeletePostButtonsProps {
  id: number
  disableBtns?: {
    delete: boolean,
    edit: boolean
  }
}

const EditDeletePostButtons: FC<EditDeletePostButtonsProps> = ({id, disableBtns}) => {
  const [, deletePost] = useDeletePostMutation();

  return (
    <Stack>
      <NextLink href={`post/edit/${id}`}>
        <IconButton aria-label={"edit"}
                    icon={<EditIcon/>} alignSelf={"flex-start"}
                    disabled={disableBtns?.edit || false}
                    colorScheme={"cyan"}
        />
      </NextLink>
      <IconButton aria-label={"delete"}
                  icon={<DeleteIcon/>} alignSelf={"flex-start"}
                  colorScheme={"red"}
                  disabled={disableBtns?.delete || false}
                  onClick={() => {
                    deletePost({id})
                  }}
      />
    </Stack>
  );
};

export default EditDeletePostButtons;
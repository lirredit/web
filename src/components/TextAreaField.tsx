import {FC, TextareaHTMLAttributes, useState} from "react";
import {useField} from "formik";
import {FormControl, FormErrorMessage, FormLabel, Textarea} from "@chakra-ui/react";

type Props = TextareaHTMLAttributes<HTMLTextAreaElement> & {
  name: string;
  label?: string;
  placeholder?: string;
};

const TextAreaField: FC<Props> = (props) => {
  //Working only in Formik form
  const [{name, placeholder, label, restProps}] = useState(() => {
    const {
      label = props.name[0].toUpperCase() + props.name.slice(1),
      placeholder = props.name,
      ...restProps
    } = props;
    return {name: props.name, label, placeholder, restProps};
  });

  const [field, {error}] = useField(props);

  return (
    <FormControl isInvalid={!!error}>
      <FormLabel htmlFor={name}>{label}</FormLabel>
      <Textarea {...field} id={name} {...restProps} placeholder={placeholder}/>
      {error && <FormErrorMessage>{error}</FormErrorMessage>}
    </FormControl>
  );
};

export default TextAreaField;

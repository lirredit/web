import {FC, useState} from "react";
import {IconButton, Stack, Text} from "@chakra-ui/react";
import {ChevronDownIcon, ChevronUpIcon} from "@chakra-ui/icons";
import {PostSnippetFragment, useVoteMutation} from "../generated/graphql";

interface UpdootSectionProps {
  post: PostSnippetFragment
}

const UpdootSection: FC<UpdootSectionProps> = ({post}) => {
  const [loading, setLoading] = useState<"up"| "down" | null | false>( null)
  const [,vote] = useVoteMutation();
  return (
    <Stack alignItems={'center'} mr={4} justifyContent={'center'}>
      <IconButton
        isLoading={loading === "up"}
        aria-label={"point up"}
        onClick={async () => {
          if (post.voteStatus === 1) {
            return
          }
          setLoading("up")
          await vote({
            postId: post.id,
            value: 1
          })
          setLoading(false)
        }}
        icon={<ChevronUpIcon w={7} h={7}/>}
        colorScheme={post.voteStatus === 1 ? "green" : undefined}
      />
      <Text>{post.points}</Text>
      <IconButton
        isLoading={loading === "down"}
        aria-label={"point down"}
        onClick={async () => {
          if (post.voteStatus === -1) {
            return
          }
          setLoading("down")
          await vote({
            postId: post.id,
            value: -1
          })
          setLoading(false)
        }}
        colorScheme={post.voteStatus === -1 ? "red" : undefined}
        icon={<ChevronDownIcon w={7} h={7}/>
        }
      />
    </Stack>
  );
};

export default UpdootSection;